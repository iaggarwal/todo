The application has been deployed on firebase and can be accessed via link - https://yetanothertodo-cca34.web.app/todo

Login to the app is supported via google only as of now or through a user that I create. For the purpose of evaluation I have created a user for Murray (You might have got activation email):

* Username - `<redacted>`
* Password - `<redacted>`

Credentials sent seperately on email 

**Note: Please note that I have not implemented as of now the logout functionality, so please open in incognito window**

# Steps to run the app locally
1. Clone the git repo
2. run `npm install`
3. run `npx ng serve`
4. open the app on - `http://localhost:4200`

The app is connected to firestore and all the todos are stored there. I did not get time to write firebase function for the Crud API, but using angular fire is better because it keeps UI in sync with the firestore