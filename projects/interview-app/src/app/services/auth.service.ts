import { Injectable } from '@angular/core';
import * as Cookies from 'js-cookie';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
  private userDetailsKey = 'user_info';
  public userInfo: any;

  public set userDetails(userDetails: any) {
    if (!userDetails) {
      Cookies.remove(this.userDetailsKey);
    } else {
      const date = new Date();
      date.setTime(date.getTime() + 24 * 60 * 60 * 1000);
      Cookies.set(this.userDetailsKey, userDetails, {
        expires: 1
      });
    }
  }

  public get userDetails(): any {
    return JSON.parse(Cookies.get(this.userDetailsKey) || '{}');
  }

  onSuccessfulLogin(userDetails: any) {
    this.userInfo = userDetails;
    this.userDetails = userDetails;
  }
}
