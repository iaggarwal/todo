import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private readonly router: Router, private readonly authService: AuthService) {}
  title = 'interview-app';

  loginSuccess(event: any) {
    console.log(event);
    this.authService.onSuccessfulLogin({uid: event.uid, email: event.email, name: event.displayName});
    this.goToToDo();
  }
  goToToDo() {
    this.router.navigate(['todo']);
  }
}
