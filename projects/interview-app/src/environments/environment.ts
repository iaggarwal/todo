// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB7HcjH_YTraO9UTCyVDwv_2EJ5Cz3DPXc',
    authDomain: 'yetanothertodo-cca34.firebaseapp.com',
    databaseURL: 'https://yetanothertodo-cca34.firebaseio.com',
    projectId: 'yetanothertodo-cca34',
    storageBucket: 'yetanothertodo-cca34.appspot.com',
    messagingSenderId: '617286369252'
  },
  cookieDomain: 'localhost'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
