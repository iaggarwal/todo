export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyB7HcjH_YTraO9UTCyVDwv_2EJ5Cz3DPXc',
    authDomain: 'yetanothertodo-cca34.firebaseapp.com',
    databaseURL: 'https://yetanothertodo-cca34.firebaseio.com',
    projectId: 'yetanothertodo-cca34',
    storageBucket: 'yetanothertodo-cca34.appspot.com',
    messagingSenderId: '617286369252'
  },
  cookieDomain: 'yetanothertodo-cca34'
};
