import { Observable, from } from 'rxjs';
import { ITodo } from './../types/todo.interface';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from 'projects/interview-app/src/app/services/auth.service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class TodoApiService {
  constructor(
    private firestore: AngularFirestore,
    private readonly authService: AuthService
  ) {}
  loadTodo = (): Observable<Array<any>> =>
    this.firestore
      .collection<Array<any>>(
        `todos/${this.authService.userDetails.email}/tasks/`
      )
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(c => {
            return { id: c.payload.doc.id, ...c.payload.doc.data() };
          });
        })
      )

  addTodo = (todo: ITodo): Observable<any> =>
    from(
      this.firestore
        .collection(`todos/${this.authService.userDetails.email}/tasks/`)
        .add(todo)
    ).pipe(
      map(val => val),
      catchError(e => e)
    )

  deleteTodo = (todo: ITodo): Observable<any> =>
    from(
      this.firestore
        .collection(`todos/${this.authService.userDetails.email}/tasks`)
        .doc(todo.id)
        .delete()
    )

  completeTodo = (todo: ITodo): Observable<any> => {
    const upadteDoc = { ...todo };
    delete upadteDoc.id;
    return from(
      this.firestore
        .collection(`todos/${this.authService.userDetails.email}/tasks`)
        .doc(todo.id)
        .set(upadteDoc)
    );
  }

  filter = (text: string): Observable<Array<ITodo>> =>
    this.loadTodo().pipe(
      map(todos => {
        return todos.map(todo => {
          return { ...todo, display: todo.task.includes(text) };
        });
      })
    )
}
