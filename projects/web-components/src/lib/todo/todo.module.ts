import { TodoEffects } from './store/todo-effects';
import { TodoComponent } from './component/todo.component';
import { TodoContainerComponent } from './container/todo.container';
import { NgModule } from '@angular/core';
import { TodoApiService } from './services/todo-api.service';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromTodo from './store/reducers/todo-reducer';
import { ReactiveFormsModule } from '@angular/forms';
import { TodoFormFieldComponent } from './component/todo-field/todo-field.component';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { CdkTableModule } from '@angular/cdk/table';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [TodoContainerComponent, TodoComponent, TodoFormFieldComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forFeature('todos', fromTodo.reducer),
    EffectsModule.forFeature([TodoEffects]),
    CdkTableModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    BrowserAnimationsModule
  ],
  exports: [TodoContainerComponent, TodoComponent, TodoFormFieldComponent],
  providers: [TodoApiService]
})
export class TodoModule {}
