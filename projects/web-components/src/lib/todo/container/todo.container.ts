import { Delete, Complete, Add } from './../store/todo-actions';
import { ITodo } from './../types/todo.interface';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromTodo from '../store/reducers';
import { State } from '../store/reducers/todo-reducer';
import { Filter, Load } from '../store/todo-actions';

@Component({
  selector: 'lib-todo-container',
  template: `
    <lib-todo
      [todos]="todo$ | async"
      (todoDeleted)="onTodoDeleted($event)"
      (todoCompleted)="onTodoCompleted($event)"
      (todoAdded)="onTodoAdded($event)"
      (todoFiltered)="onTodoFiltered($event)"
    ></lib-todo>
  `
})
export class TodoContainerComponent implements OnInit {
  todo$: Observable<Array<ITodo>>;
  constructor(private readonly store$: Store<State>) {}
  ngOnInit(): void {
    this.todo$ = this.store$.select(fromTodo.getTodos);

    this.store$.dispatch(new Load());
  }

  onTodoDeleted = (todo: ITodo) => this.store$.dispatch(new Delete(todo));
  onTodoCompleted = (todo: ITodo) => this.store$.dispatch(new Complete(todo));
  onTodoAdded = (todo: ITodo) => {
    this.store$.dispatch(new Add(todo));
  }
  onTodoFiltered = (text: string) => this.store$.dispatch(new Filter(text));
}
