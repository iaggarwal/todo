import { TodoApiService } from './../services/todo-api.service';
import { ITodo } from './../types/todo.interface';
import { TodoActions, Load,
   LoadSuccess, LoadFailure, Add,
    AddSuccess, AddFailure, Delete,
     DeleteSuccess, DeleteFailure, Complete,
      CompleteSuccess, CompleteFailure } from './todo-actions';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Filter, FilterSuccess } from './todo-actions';

@Injectable()
export class TodoEffects {
  constructor(private actions$: Actions, private todoApiService: TodoApiService) {}

  @Effect()
  load$ = this.actions$.pipe(
    ofType(TodoActions.Load),
    switchMap((action: Load) => {
      return this.todoApiService.loadTodo().pipe(
        map((response: Array<ITodo>) => {
          return new LoadSuccess(response);
        }),
        catchError((e) => {
          return of(new LoadFailure('Some error'));
        })
      );
    })
  );

  @Effect({dispatch: false})
  add$ = this.actions$.pipe(
    ofType(TodoActions.Add),
    switchMap((action: Add) => {
      return this.todoApiService.addTodo(action.payload).pipe(
        map(() => {
          console.log('Added successfully');
        }),
        catchError(() => {
          return of(new AddFailure('Some error'));
        })
      );
    })
  );

  @Effect({dispatch: false})
  delete$ = this.actions$.pipe(
    ofType(TodoActions.Delete),
    switchMap((action: Delete) => {
      return this.todoApiService.deleteTodo(action.payload).pipe(
        map(() => {
          console.log('Deleted successfully');
        }),
        catchError(() => {
          return of(new DeleteFailure('Some error'));
        })
      );
    })
  );

  @Effect({dispatch: false})
  Complete$ = this.actions$.pipe(
    ofType(TodoActions.Complete),
    switchMap((action: Complete) => {
      return this.todoApiService.completeTodo(action.payload).pipe(
        map(() => {
          console.log('Completed successfully');
        }),
        catchError(() => {
          return of(new CompleteFailure('Some error'));
        })
      );
    })
  );

  @Effect()
  filter$ = this.actions$.pipe(
    ofType(TodoActions.Filter),
    switchMap((action: Filter) => {
      return this.todoApiService.filter(action.payload).pipe(
        map((response: Array<ITodo>) => {
          return new FilterSuccess(response);
        }),
        catchError(() => {
          return of(new CompleteFailure('Some error'));
        })
      );
    })
  );
}
