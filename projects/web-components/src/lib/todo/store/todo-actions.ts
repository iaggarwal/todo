import { ITodo } from './../types/todo.interface';
import { Action } from '@ngrx/store';

export enum TodoActions {
  Load = '[Todo] LoadTodo',
  LoadSuccess = '[Todo] LoadTodoSuccess',
  LoadFailure = '[Todo] LoadTodoFailure',

  Complete = '[Todo] Complete',
  CompleteSuccess = '[Todo] CompleteSuccess',
  CompleteFailure = '[Todo] CompleteFailure',

  Add = '[Todo] Add',
  AddSuccess = '[Todo] AddSuccess',
  AddFailure = '[Todo] AddFailure',

  Delete = '[Todo] Delete',
  DeleteSuccess = '[Todo] DeleteSuccess',
  DeleteFailure = '[Todo] DeleteFailure',

  Filter = '[Todo] Filter',
  FilterSuccess = '[Todo] FilterSuccess'
}

export class Load implements Action {
  readonly type = TodoActions.Load;
}

export class LoadSuccess implements Action {
  readonly type = TodoActions.LoadSuccess;
  constructor(public payload: Array<ITodo>) {}
}

export class LoadFailure implements Action {
  readonly type = TodoActions.LoadFailure;
  constructor(public payload: string) {}
}

export class Filter implements Action {
  readonly type = TodoActions.Filter;
  constructor(public payload: string) {}
}

export class FilterSuccess implements Action {
  readonly type = TodoActions.FilterSuccess;
  constructor(public payload: Array<ITodo>) {}
}

export class Add implements Action {
  readonly type = TodoActions.Add;
  constructor(public payload: ITodo) {}
}

export class AddSuccess implements Action {
  readonly type = TodoActions.AddSuccess;
  constructor(public payload: Array<ITodo>) {}
}

export class AddFailure implements Action {
  readonly type = TodoActions.AddFailure;

  constructor(public payload: string) {}
}

export class Complete implements Action {
  readonly type = TodoActions.Complete;
  constructor(public payload: ITodo) {}
}

export class CompleteSuccess implements Action {
  readonly type = TodoActions.CompleteSuccess;
  constructor(public payload: Array<ITodo>) {}
}

export class CompleteFailure implements Action {
  readonly type = TodoActions.CompleteFailure;

  constructor(public payload: string) {}
}

export class Delete implements Action {
  readonly type = TodoActions.Delete;
  constructor(public payload: ITodo) {}
}

export class DeleteSuccess implements Action {
  readonly type = TodoActions.DeleteSuccess;
  constructor(public payload: Array<ITodo>) {}
}

export class DeleteFailure implements Action {
  readonly type = TodoActions.DeleteFailure;

  constructor(public payload: string) {}
}

export type Actions =
  | Load
  | LoadSuccess
  | LoadFailure
  | Filter
  | FilterSuccess
  | Complete
  | CompleteSuccess
  | CompleteFailure
  | Delete
  | DeleteSuccess
  | DeleteFailure
  | Add
  | AddSuccess
  | AddFailure;
