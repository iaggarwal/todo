import { State } from './todo-reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';
const getTodoState = createFeatureSelector<State>('todos');

export const getTodos = createSelector(getTodoState, (state: State) => {
  return state && state.todo ? state.todo : [];
});
export const getLoading = createSelector(
  getTodoState,
  (state: State) => state && state.loading
);
export const getError = createSelector(
  getTodoState,
  (state: State) => state && state.error
);
export const getErrorMessage = createSelector(
  getTodoState,
  (state: State) => state && state.errorMessage
);

export const getFilteredTodos = createSelector(
  getTodoState,
  (state: State) => state && state.filteredTodo
);
