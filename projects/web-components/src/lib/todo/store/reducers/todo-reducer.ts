import { Actions, TodoActions } from '../todo-actions';
import { ITodo } from '.././../types/todo.interface';
export interface State {
  loading: boolean;
  error: boolean;
  todo: Array<ITodo>;
  filteredTodo: Array<ITodo>;
  errorMessage: string;
}

export const initialState: State = {
  loading: false,
  todo: [],
  filteredTodo: [],
  error: false,
  errorMessage: ''
};

export function reducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case TodoActions.Load: {
      return {
        ...state,
        loading: true,
        todo: []
      };
    }
    case TodoActions.LoadSuccess:
    case TodoActions.AddSuccess:
    case TodoActions.CompleteSuccess:
    case TodoActions.FilterSuccess:
    case TodoActions.DeleteSuccess: {
      return {
        ...state,
        loading: false,
        todo: [...action.payload]
      };
    }
    case TodoActions.LoadFailure:
    case TodoActions.AddFailure:
    case TodoActions.CompleteFailure:
    case TodoActions.DeleteFailure: {
      return {
        ...state,
        error: true,
        errorMessage: action.payload
      };
    }
    default:
      return { ...state };
  }
}
