export interface ITodo {
  id?: string,
  task: string;
  createdOn: string;
  isCompleted: boolean;
  display: boolean;
  completedOn: string;
}
