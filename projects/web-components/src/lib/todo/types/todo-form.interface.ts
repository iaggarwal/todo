import { ITodo } from './todo.interface';
export interface ITodoForm {
  todo: ITodo;
}