import { ITodo } from './../types/todo.interface';
import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { ITodoForm } from '../types/todo-form.interface';

@Component({
  selector: 'lib-todo',
  styleUrls: ['./todo.component.scss'],
  templateUrl: './todo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoComponent implements OnInit, OnChanges {
  @Input()
  todos: Array<ITodo>;

  tableData: Array<ITodo>;

  @Output()
  todoDeleted = new EventEmitter<ITodo>();
  @Output()
  todoCompleted = new EventEmitter<ITodo>();
  @Output()
  todoAdded = new EventEmitter<ITodo>();
  @Output()
  todoFiltered = new EventEmitter<string>();

  todoForm = this.fb.group({
    todo: [{}, Validators.required]
  });

  reset = {
    value: false
  };

  displayedColumns: Array<string> = [
    'task',
    'createdOn',
    'completedOn',
    'actions'
  ];

  todoToBeAdded: ITodo = {} as ITodo;

  get activeTodo() {
    return this.todos.filter(todo => !todo.isCompleted);
  }

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit(): void {
    this.todoForm.valueChanges
      .pipe(
        map((value: ITodoForm) => {
          this.todoToBeAdded = value.todo;
        })
      )
      .subscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.todos && changes.todos.currentValue) {
      this.tableData = this.todos.filter(todo => todo.display);
    }
  }

  onAddTodo = (todo: ITodo) => {
    this.todoAdded.emit(todo);
    this.reset = {...this.reset, value: true};
  }
  onDeleteTodo = (todo: ITodo) => {
    this.todoDeleted.emit(todo);
  }
  onCompleteTodo = (todo: ITodo) => {
    this.todoCompleted.emit({
      ...todo,
      isCompleted: true,
      completedOn: new Date().toDateString()
    });
  }
  onTodoFiltered = (event: any) => {
    this.todoFiltered.emit(event.target.value);
  }
}
