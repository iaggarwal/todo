import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { ITodo } from '../../types/todo.interface';
import {
  FormBuilder,
  ControlValueAccessor,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
  FormControl
} from '@angular/forms';
import { noop } from 'rxjs';
import { OnInit } from '@angular/core';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'lib-todo-form-field',
  templateUrl: './todo-field.component.html',
  styleUrls: ['./todo-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TodoFormFieldComponent),
      multi: true
    }
  ]
})
export class TodoFormFieldComponent
  implements OnInit, OnChanges, ControlValueAccessor {
  @Input()
  reset: object;
  @Output()
  addTodo: EventEmitter<ITodo> = new EventEmitter<ITodo>();
  taskField: FormControl = new FormControl('');

  private todo: ITodo;

  get taskValue() {
    return this.taskField.value;
  }

  set taskValue(value: string) {
    this.taskField.setValue(value);
  }

  touched: boolean;
  constructor(private readonly fb: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.reset && changes.reset.currentValue.value.toString() === 'true') {
      this.taskField.reset();
    }
  }
  ngOnInit() {
    this.taskField.valueChanges
      .pipe(
        map((value: string) => {
          if (value) {
            this.onChange({
              task: value,
              isCompleted: false,
              createdOn: new Date().toDateString(),
              completedOn: '',
              display: true
            });
          }
        })
      )
      .subscribe();
  }
  onChange = (delta: any) => {};

  onTouched = () => {
    this.touched = true;
  }

  writeValue(value: ITodo): void {
    this.todo = value;
    this.taskValue = this.todo.task;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.taskField.disable() : noop();
  }
}
